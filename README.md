# Makerdao SES Dashboard

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) TS template.
### After bootstrapping it was also added:
- Storybook
- ESLint
- Apollo Client
- Material UI, emotion and styled components
- Formik and YUP
- Sass
- React-router-dom

## Getting started

### To run in development mode use:
### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### To run tests use: 
### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### To build the app for production use:
### `yarn build`
